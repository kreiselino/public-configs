# ~/.profile: executed by the command interpreter for login shells.
# This file is not read by bash(1), if ~/.bash_profile or ~/.bash_login
# exists.
# see /usr/share/doc/bash/examples/startup-files for examples.
# the files are located in the bash-doc package.

# the default umask is set in /etc/profile; for setting the umask
# for ssh logins, install and configure the libpam-umask package.
#umask 022

# if running bash
if [ -n "$BASH_VERSION" ]; then
    # include .bashrc if it exists
    if [ -f "$HOME/.bashrc" ]; then
	. "$HOME/.bashrc"
    fi
fi

# See if directory exists and add it to path
for d in \
    "$HOME/bin" \
    "$HOME/.local/bin" \
    "$HOME/.yarn/bin" \
    "$HOME/.npm-global/bin" \
    "$HOME/.rbenv/bin" \
    "$HOME/.rvm/bin" \
    "$HOME/.rd/bin" \
    "/opt/intellij-idea-ultimate/bin" \
    "/usr/local/opt/libpq/bin" \
    "/opt/homebrew/opt/gnu-sed/libexec/gnubin" \
    "/opt/conda/bin"; do
    if [ -d "$d" ] ; then
        export PATH="$d:$PATH"
    fi
done

# This is for homebrew if it exists
for f in \
    "/opt/homebrew/bin/brew" \
    "/usr/local/bin/brew"; do
    if [ -f "$f" ] ; then
        eval "$($f shellenv)"
    fi
done

# This is for the gitpod docker image
if [ -d "$HOME/.cargo" ] ; then
    . $HOME/.cargo/env
fi

# set PATH and correct OpenSSL so it includes user's rbenv path
if [ -d "$HOME/.rbenv" ] ; then
    [[ ! -f $HOMEBREW_PREFIX/bin/brew ]] || export RUBY_CONFIGURE_OPTS="--with-openssl-dir=$(brew --prefix openssl@1.1)"
    eval "$(rbenv init -)"
fi

# set environment if pyenv is installed
if command -v pyenv 2>&1 >/dev/null ; then
    eval "$(pyenv init -)"
fi
if command -v pyenv-virtualenv-init 2>&1 >/dev/null ; then
    eval "$(pyenv virtualenv-init -)"
fi

# set GRADLE_USER_HOME if it exists
if [ -d "$HOME/.gradle" ] ; then
    export GRADLE_USER_HOME="$HOME/.gradle"
fi

# set PATH and correct OpenSSL so it includes user's rbenv path
if [ -f "/usr/local/bin/rclone----" ] ; then
    rclone mount MyWork: ~/MyWork --daemon --vfs-cache-mode full
    rclone mount MyDrive: ~/MyDrive --daemon --vfs-cache-mode full
    rclone mount MyShared: ~/MyShared --daemon --vfs-cache-mode full
    rclone mount GoogleDrive: ~/GoogleDrive --daemon --vfs-cache-mode full
    rclone mount OneDrive: ~/OneDrive --daemon --vfs-cache-mode full
    rclone mount S3Devan:devan-s3-mount ~/S3Devan --daemon --vfs-cache-mode full
fi

# do some machine specific initialization
if [ -f "$HOME/.profile_local" ]; then
  . "$HOME/.profile_local"
fi
