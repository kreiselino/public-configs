######################################################################################################################
#
# Initialization of ZSH by Andreas Kreisel (andreas.kreisel@live.de)
#
#######################################################################################################################

# set tty size if we are logged in from serial console
[[ $TTY != "ttyS*" ]] || stty rows 140 cols 50

# Some constants
export P10K_CACHE_HOME="${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
export SDKMAN_DIR="$HOME/.sdkman"
export AUTOENV_FILE_LEAVE=$AUTOENV_FILE_ENTER
export ALTERNATE_EDITOR=""
export EMACS_CLIENT="$HOME/.antigen/bundles/robbyrussell/oh-my-zsh/plugins/emacs/emacsclient.sh"

# Here we do some host dependent stuff
case $HOST in
    'HANCOCK')
        export DISPLAY=$(awk '/nameserver / {print $2; exit}' /etc/resolv.conf 2>/dev/null):0
        export LIBGL_ALWAYS_INDIRECT=1
        ;;
esac

# and some os dependent stuff
case $OSTYPE in
    linux-gnu)
        export LS_LITERAL="-N"
        export LS_COLOR="--color=tty"
        export ANTIGEN_SOURCE=/usr/share/zsh-antigen/antigen.zsh
        alias yarn=yarnpkg
        ;;
    darwin2*)
        export LS_LITERAL=""
        export LS_COLOR="-G"
        export CLICOLORS=1
        # This should only be used on osx
        export RUBY_CONFIGURE_OPTS="--with-openssl-dir=$(brew --prefix openssl@1.1)"
        export ANTIGEN_SOURCE=$HOMEBREW_CELLAR/antigen/2.2.3/share/antigen/antigen.zsh
        export ALTERNATE_EDITOR="/opt/homebrew/bin/emacs"
        ;;
    *)
        export LS_LITERAL=""
        export LS_COLOR="-G"
        export CLICOLORS=1
        ;;
esac

# show my banner if existing
[[ ! -r ~/.banner ]] || source ~/.banner

# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
[[ ! -r "$P10K_CACHE_HOME" ]] || source "$P10K_CACHE_HOME"

# Use antigen ans plugin manager
[[ ! -r "$ANTIGEN_SOURCE" ]] || source "$ANTIGEN_SOURCE"

# Some autocompletion stuff
zstyle ':completion:*' menu select
autoload -Uz compinit && compinit

# Load the oh-my-zsh's library.
antigen use oh-my-zsh

antigen theme romkatv/powerlevel10k

# Bundles from the default repo (robbyrussell's oh-my-zsh).
antigen bundle git
antigen bundle docker
antigen bundle emacs
antigen bundle sdk
antigen bundle vscode
antigen bundle aws
antigen bundle lein
antigen bundle conda
antigen bundle ssh
antigen bundle command-not-found

antigen bundle agkozak/zsh-z
antigen bundle Tarrasch/zsh-autoenv
antigen bundle unixorn/autoupdate-antigen.zshplugin
antigen bundle zsh-users/zsh-autosuggestions
antigen bundle zsh-users/zsh-completions
antigen bundle zsh-users/zsh-syntax-highlighting

# See if we need sdk bundle

[[ ! -f ""$SDKMAN_DIR/bin/sdkman-init.sh"" ]] || antigen bundle matthieusb/zsh-sdkman

# Tell Antigen that you're done.
antigen apply

# User configuration

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh

# And now sourcing conda if there
[[ ! -f /opt/conda/etc/profile.d/conda.sh ]] || source /opt/conda/etc/profile.d/conda.sh

# Integration of zsh and iTerm2
[[ ! -f ~/.iterm2_shell_integration.zsh ]] || source ~/.iterm2_shell_integration.zsh

# See if we're in a wsl and windows profile is mounted
[[ ! -d /mnt/c/Users/andreas ]] || export HHOME=/mnt/c/Users/andreas

# Preferred editor for local and remote sessions at least
# if oh-my-zsh plugin is available
if [[ -n $SSH_CONNECTION && ! -r "$EMACS_CLIENT" ]]; then
    export EDITOR='emacs'
else
    export EDITOR="$EMACS_CLIENT"
fi

# We update all our packages every seven days (on mac)
if hash brew &> /dev/null && test -f ~/.brewfile; then
    [[ -f ~/.brew.lastrun ]] || echo 0 > ~/.brew.lastrun
    if [[ $(expr $(date +%s) - $(cat ~/.brew.lastrun)) -gt $(expr 60 \* 60 \* 24 \* 7) ]]; then
        brew upgrade --greedy
        brew bundle install --file=~/.brewfile
        date +%s > ~/.brew.lastrun
    fi
fi

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#

#### Simple aliases
alias ls='ls $LS_COLOR $LS_LITERAL'
alias lsa='ls -lah $LS_LITERAL'
alias l='ls -ah $LS_LITERAL'
alias ll='ls -lh $LS_LITERAL'
alias la='ls -lAh $LS_LITERAL'
[[ -z $HHOME ]] || alias cdh="cd $HHOME"

#### Parametrized aliases
function cdc() {cd $HOME/Customer/$1}
function psg() {[[ -z $(pgrep -i "$1") ]] || ps -o user,pid,%cpu,%mem,vsz,rss,tt,state,start,time,command -p $(pgrep -i "$1")| numfmt --header --from-unit=1024 --to=iec --field 5-6}
function s() {
    case $1 in
        opnsense)
            ssh root@opnsense.home.diekreisels.de
            ;;
        raspif)
            ssh pi@raspif.home.diekreisels.de
            ;;
        nas)
            ssh -p 73 root@nas.home.diekreisels.de
            ;;
    esac
}
function gi() {
    curl -sL https://www.toptal.com/developers/gitignore/api/$@;
}
