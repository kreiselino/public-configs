// https://gitlab.com/kreiselino/public-configs/-/raw/main/surfingkeys/settings.js
api.map('<Ctrl-q>', '<Alt-s>');

api.Hints.style('border: solid 1px #555555; color:#FFFFFF; background: none; background-color: #555555; font-family: Input Sans Condensed, Charcoal, sans-serif; opacity:0.7;');

settings.aceKeybindings = "emacs";
settings.startToShowEmoji = 2;
settings.tabsThreshold = 0;
settings.hintAlign = "left";
settings.lurkingPattern = /.*gitlab.com|.*ica-group.atlassian.net|.*gitlab-blade.ica-service.de|.*console.aws.amazon.com/i
settings.smoothScroll = true;
settings.omnibarMaxResults = 20;
settings.stealFocusOnLoad = true;

// set theme
settings.theme = `
.sk_theme {
    font-family: Input Sans Condensed, Charcoal, sans-serif;
    font-size: 10pt;
    background: #24272e;
    color: #abb2bf;
}
.sk_theme tbody {
    color: #fff;
}
.sk_theme input {
    color: #d0d0d0;
}
.sk_theme .url {
    color: #61afef;
}
.sk_theme .annotation {
    color: #56b6c2;
}
.sk_theme .omnibar_highlight {
    color: #528bff;
}
.sk_theme .omnibar_timestamp {
    color: #e5c07b;
}
.sk_theme .omnibar_visitcount {
    color: #98c379;
}
.sk_theme #sk_omnibarSearchResult ul li:nth-child(odd) {
    background: #303030;
}
.sk_theme #sk_omnibarSearchResult ul li.focused {
    background: #3e4452;
}
#sk_status, #sk_find {
    font-size: 20pt;
}`;
// click `Save` button to make above settings to take effect.</ctrl-i></ctrl-y>
