(require 'init-lsp)

(use-package graphql-mode)

(provide 'init-graphql)
