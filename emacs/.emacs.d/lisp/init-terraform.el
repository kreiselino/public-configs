(use-package terraform-mode
  :mode "\\.tf$"
  :interpreter "terraform")

(provide 'init-terraform)
